# ava-technical-test

### Demo
[```https://ava.ali-murad.com/```](https://ava.ali-murad.com/)

### Project setup
```npm install```

### Run the project
```npm start```

### Compiles and minifies for production
```npm run build```

### Highlights / Extra points

Sass is being used for styling


