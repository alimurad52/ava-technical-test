import React, {useState} from 'react';
import {Slider} from "../component/Slider";
import {Grid} from "../component/Grid";
import {ColorPicker} from "../component/ColorPicker";

const initialGrid = [
    [0, 0, 0, 0, 1],
    [1, 1, 0, 0, 0],
    [1, 1, 0, 1, 1],
    [0, 0, 0, 0, 0],
    [1, 1, 1, 0, 0]
];

export const LandingPage = () => {

    const [sliderVal, setSliderVal] = useState(5);
    const [oriTwoDArray, setOriTwoDArray] = useState(initialGrid);
    const [twoDArray, setTwoDArray] = useState(initialGrid);
    const [boxColor, setBoxColor] = useState('#96433D');
    const [selectionBoxColor, setSelectionBoxColor] = useState('#FF7A6363');
    const [numberOfConnectedBox, setNumberOfConnectedBox] = useState(0);
    const [clickedBox, setClickedBox] = useState([]);

    const onChangeSlider = (e) => {
        const value = parseInt(e.target.value);
        let arr = new Array(value)
            .fill(0)
            .map(() => (new Array(value)
                .fill(0)
                .map(() => Math.round(Math.random()))));
        setNumberOfConnectedBox('');
        setTwoDArray(arr);
        setOriTwoDArray(arr);
        setSliderVal(value);
    };

    const findConnectedItems = (y, x, z) => {
        //check if there is a box above, right, left and down to the passed parameters (y, x)
        let up = (y - 1 >= 0), down = (y + 1 < z.length), right = (x + 1 < sliderVal), left = (x - 1 >= 0);
        let tempUp = 0, tempDown = 0, tempRight = 0, tempLeft = 0;

        //mark the ones which have gone through the function
        z[y][x] = "checked";

        //check the box on the up, down, left and right of the clicked box
        //if it is also carrying the value 1
        if(up && z[y-1][x] === 1) {
            //takes the count of all boxes matched above to 1
            tempUp = findConnectedItems(y-1, x, z);
        }
        if(down && z[y+1][x] === 1) {
            //takes the count of all boxes matched below to 1
            tempDown = findConnectedItems(y+1, x, z);
        }
        if(left && z[y][x-1] === 1) {
            //takes the count of all boxes matched on the left to 1
            tempLeft = findConnectedItems(y, x-1, z);
        }
        if(right && z[y][x+1] === 1) {
            //takes the count of all boxes matched on the right to 1
            tempRight = findConnectedItems(y, x+1, z);
        }
        setTwoDArray(z);
        return tempUp + tempLeft + tempRight + tempDown + 1;
    };

    const onClickBox = (e, y, x, val) => {
        const copy = JSON.parse(JSON.stringify(oriTwoDArray));
        switch (val) {
            case 1:
                setClickedBox([y, x]);
                setNumberOfConnectedBox(findConnectedItems(y, x, copy));
                break;
            case 'checked':
                setClickedBox([y, x]);
                setNumberOfConnectedBox(findConnectedItems(y, x, copy));
                break;
            default:
                setTwoDArray(oriTwoDArray);
                setClickedBox([]);
                setNumberOfConnectedBox(0);
        }
    };

    const onChangeBoxColor = (color) => {
        setBoxColor(color.hex);
    };

    const onChangeSelectionBoxColor = (color) => {
        setSelectionBoxColor(color.hex);
    };

    const onMouseEnter = (e, y, x, val) => {
        const copy = JSON.parse(JSON.stringify(oriTwoDArray));
        switch (val) {
            case 1:
            case 'checked':
                findConnectedItems(y, x, copy);
                break;
            default:
                setTwoDArray(oriTwoDArray);
                break;
        }
    };

    const onMouseExit = () => {
        setTwoDArray(oriTwoDArray);
    };

    return (
        <div className="container-wrapper">
            <div className="slider-color-wrapper">
                <Slider
                    onChangeSlider={onChangeSlider}
                    value={sliderVal}
                />
                <ColorPicker
                    boxColor={boxColor}
                    selectionBoxColor={selectionBoxColor}
                    onChangeBoxColor={onChangeBoxColor}
                    onChangeSelectionBoxColor={onChangeSelectionBoxColor}
                />
            </div>
            <Grid
                onMouseExit={onMouseExit}
                clickedBox={clickedBox}
                numberOfConnectedBoxes={numberOfConnectedBox}
                boxColor={boxColor}
                selectionBoxColor={selectionBoxColor}
                array={twoDArray}
                onClickBox={onClickBox}
                onMouseEnter={onMouseEnter}
            />
        </div>
    )
}