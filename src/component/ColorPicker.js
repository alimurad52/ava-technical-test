import React, {useState} from 'react';
import { SketchPicker } from 'react-color'

export const ColorPicker = (
    {
        boxColor,
        selectionBoxColor,
        onChangeBoxColor,
        onChangeSelectionBoxColor
    }) => {

    const [showBoxColorPicker, setShowBoxColorPicker] = useState(false);
    const [showSelectionColorPicker, setShowSelectionColorPicker] = useState(false);

    return (
        <div className="color-picker-wrapper">
            <div className="color-wrapper">
                <p>Box color</p>
                <div className="color-palette-wrapper">
                    <div
                        className="color-palette"
                        style={{backgroundColor: boxColor}}
                        onClick={() => setShowBoxColorPicker(!showBoxColorPicker)}
                    />
                </div>
                {
                    showBoxColorPicker &&
                    <SketchPicker
                        presetColors={[]}
                        color={boxColor}
                        onChangeComplete={onChangeBoxColor}
                    />
                }
            </div>
            <div className="color-wrapper">
                <p>Hover color</p>
                <div className="color-palette-wrapper">
                    <div
                        className="color-palette"
                        style={{backgroundColor: selectionBoxColor}}
                        onClick={() => setShowSelectionColorPicker(!showSelectionColorPicker)}
                    />
                </div>
                {
                    showSelectionColorPicker &&
                    <SketchPicker
                        presetColors={[]}
                        color={selectionBoxColor}
                        onChangeComplete={onChangeSelectionBoxColor}
                    />
                }
            </div>
        </div>
    )
}

