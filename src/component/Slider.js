import React from 'react';

export const Slider = ({onChangeSlider, value}) => {
    return (
        <div className="slider-wrapper">
            <input
                type="range"
                value={value}
                onChange={onChangeSlider}
                className="slider"
                max={20}
                min={5}
            />
            <p>{value}</p>
        </div>
    )
}