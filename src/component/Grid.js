import React from 'react';

export const Grid = (
    {
        array,
        onClickBox,
        boxColor,
        numberOfConnectedBoxes,
        clickedBox,
        selectionBoxColor,
        onMouseEnter,
        onMouseExit
    }) => {

    const isWith1 = (x) => {
        return x === 1;
    };

    const isChecked = (x) => {
        return x === 'checked';
    };

    const setColor = (e) => {
        switch (e) {
            case 1:
                return boxColor;
            case 'checked':
                return selectionBoxColor;
            default:
                break;
        }
    };

    return (
        <div className="grid-container">
            <div className="grid-wrapper">
                {
                    array.map((y, k) => {
                        return <div className="box-wrapper" key={k}>
                            {
                                y.map((x, j) => {
                                    return (
                                        <div
                                            className={'box ' + (isWith1(x) || isChecked(x) ? 'colored-box' : "")}
                                            style={{backgroundColor: setColor(x, k, j)}}
                                            onMouseEnter={(e) => onMouseEnter(e, k, j, x)}
                                            onMouseLeave={onMouseExit}
                                            onClick={(e) => onClickBox(e, k, j, x)}
                                            key={j}
                                        >
                                            {(clickedBox[0] === k && clickedBox[1] === j) && numberOfConnectedBoxes}
                                        </div>)
                                })
                            }
                            <br />
                        </div>
                    })
                }
            </div>
        </div>
    )
}