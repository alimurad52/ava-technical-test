import React from 'react';
import ReactDOM from 'react-dom';
import './styles.scss';
import {LandingPage} from "./layout/LandingPage";

ReactDOM.render(
  <React.StrictMode>
    <LandingPage />
  </React.StrictMode>,
  document.getElementById('root')
);
